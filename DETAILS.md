ReHydrating Past Events
=======================

###### Process Overview

![flowchart](images/flowchart.png)

###### Configuration > AWS > S3 Buckets

| bucket   | description                                             |
|----------|---------------------------------------------------------|
| d1c7d0a8 | _the bucket that Datadog will archive ingested logs to_ |
| 7c5fa03b | _the bucket that Datadog will read archived logs from_  |

While it is possible to use the same bucket, **it is better practice to use distinct buckets** for a number of reasons :

- Using distinct source / destination buckets guarantees that the bucket containing the **archives created by Datadog remains "pristine"**.

- Leaving their contents untouched / unmodified **ensures they can be used as the "source of truth"**.

- It also **provides more flexibility** later on in the process :

  - Should the S3-triggered Lambda post-processing fail for some reason, it remains possible to entirely wipe the target bucket and start over and re-process all events from the source bucket.

**If using distinct buckets is inconvenient**, it remains possible to use a single bucket but then **use distinct subdirectories in S3** _( this would obey the `optional_path` configuration in the Datadog Log Archives Configuration section )_

However note that **this requires slightly more attention to details, especially regard to the Lambda IAM Role WriteS3Object authorization**.

One should then explicitly specify / ensure that the Lambda can :

- READ  from `s3://my-only-bucket/path-to-orginal-dd-archives/*`
- WRITE to   `s3://my-only-bucket/path-to-processed-dd-archives/*`

###### Configuration > AWS > Lambda Function / S3 Trigger

#### DataDog

##### Pipelines

If you have many pipelines set for ingestion in Datadog, **a good practice would be to somehow distinguish Live Logs from Historical Logs**.

This is **especially important if you are using the date_mapper filter**.

This is because the lambda that will post-process logs will require specific attributes to exist in the JSON events to determine wether or not a specific log event should be processed.

##### Log Archives

| config | archival | rehydration |
|--------|----------|-------------|
| filter |    *     |    -*       |
| bucket | d1c7d0a8 |   7c5fa03b  |

###### Config Overview in UI

![flowchart](images/dd-logs-config-archives-0.png)

###### Archive Config

![dd-logs-config-archives-1-archival](images/dd-logs-config-archives-1-archival.png)

###### Rehydrate Config

![dd-logs-config-archives-2-rehydration](images/dd-logs-config-archives-2-rehydration.png)

##### Misc. findings

###### Log Archives naming

Datadog is actually able to rehydrate archives using a static / generic archive name such as `archive.json.gz`, providing :

- The S3 path to the archive does reflect the log's timestamp ( _/dt=YYYYMMDD/hour=HH/archive.json.gz_ )
- The `date` attribute does match the S3 path

###### Handling Ingestion Delays : ingestion date vs. user-defined timestamp

When ingesting logs Datadog will use the timestamp of ingestion as the `date` attribute.

However, **using `@timestamp` during initial ingestion** actually works and **enforces the exact date set in the log** _( providing the offset with the current time isn't too large )_

This can be demonstrated like so :

    DATE=$( gdate -u "+%Y-%m-%dT%H:%M:%S.%NZ" | cut -c1-23,30 )

    JSON=$(
    cat << HEREDOC
    {
        "ddsource": "nginx",
        "hostname": "host-${RANDOM}",
        "@timestamp": "${DATE}",
        "message": "${RANDOM}"
    }
    HEREDOC
    )

    echo "${JSON}"
    echo "${DATE}"

    sleep 15

    curl -X POST "https://http-intake.logs.datadoghq.com/v1/input" -d "${JSON}"

    JSON_NO_TS=$(
    cat << HEREDOC
    {
        "ddsource": "nginx",
        "hostname": "host-${RANDOM}",
        "message": "${RANDOM}"
    }
    HEREDOC
    )


    echo "${JSON_NO_TS}"

    gdate -u "+%Y-%m-%dT%H:%M:%S.%NZ" | cut -c1-23,30
    curl -X POST "https://http-intake.logs.datadoghq.com/v1/input" -d "${JSON_NO_TS}"

    echo

In Datadog, the log event is indeed timestamped using "2020-07-16T10:52:55.354Z" _( from @timestamp )_ instead of the time of ingestion _( 15 seconds later as per `sleep 15` command )_

![example-ship-log-with-timestamp](images/example-ship-log-with-timestamp.png)

As we can see above - **despite the simulated 15 seconds ingestion delay - the log that was shipped with a `@timestamp` attributes gets displayed at the exact time described by its timestamp attribute**.

![example-ship-log-without-timestamp](images/example-ship-log-without-timestamp.png)
