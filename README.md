dd-rehydrate-past
=================

#### Description

- Parse `source_bucket` for log events archives matching `.*/archive_.*.json.gz`
	- Optionally, restrict to `YYYYMMDD` / `HH`

- Parse log archives looking for `JSON` log events

- Process log events by updating `date` with `original_timestamp`

- Write processed & sorted log events to hourly archives in `target_bucket`

Depends on `boto3`

You can find a demo video for this here : https://a.cl.ly/o0uv0XOD

#### Basic Usage

	# python3 dd-rehydrate-past.py source_bucket target_bucket

#### Available Options

| option          | description           | required |
|-----------------|-----------------------|----------|
| `source_bucket` | *bucket to read from* | **Y**    |
| `target_bucket` | *bucket to write to*  | **Y**    |
| `YYYYMMDD`      | *filter by day*       |   N      |
| `HH`            | *filter by hour*      |   N      |

#### Advanced Usage

###### Filtered by day

	# python3 dd-rehydrate-past.py source_bucket target_bucket YYYYMMDD

###### Filtered by day & hour

	# python3 dd-rehydrate-past.py source_bucket target_bucket YYYYMMDD HH

###### Filtered using RegEx

	# python3 dd-rehydrate-past.py source_bucket target_bucket YYYY.*

###### Debug Output

	# DEBUG=true python3 dd-rehydrate-past.py source_bucket target_bucket

- All **DEBUG** information is gets written **to stderr**
- **stdout** only **returns JSON**

###### Examples

*Example#1*

	# DEBUG=true python3 dd-rehydrate-past.py d1c7d0a8 7c5fa03b 20200714 09
	TOTAL OBJECTS : 2
	READ s3://d1c7d0a8/dt=20200714/hour=09/archive_090520.5947.y7A1F0KAQa-xDePWJH757A.json.gz
	TEXT LINES COUNT : 1
	READ s3://d1c7d0a8/dt=20200714/hour=09/archive_090523.7324.IO5bcdvcRIKMHZEV7q0yDw.json.gz
	TEXT LINES COUNT : 1
	PROCESSED LINES COUNT : 2
	CREATING s3://7c5fa03b/dt=20200115/hour=09/archive.json.gz
	{"_id": "AXNMkkGJ2h5H-KmRnQAA", "date": "2020-01-15T09:05:19.000Z", "service": "checkout", "host": "sesame-angles", "message": "Consonant Anteater Zoning", "status": "info", "source": "nginx", "attributes": {"env": "dev", "duration": "1.9793", "hostname": "sesame-angles", "provider": "azure", "service": "checkout", "id": "9a392359", "region": "eu-east-1", "operation": "update"}}
	{"_id": "AXNMkkiLncRhsC1IjQAA", "date": "2020-01-15T09:05:21.000Z", "service": "checkout", "host": "devotion-scope", "message": "Gout Blatantly Unify", "status": "info", "source": "postgresql", "attributes": {"env": "int", "duration": 1170800.0, "hostname": "devotion-scope", "provider": "gcp", "service": "checkout", "id": "493a8c71", "region": "eu-east-1", "operation": "read"}}
	#

*Example#2*

	# DEBUG=true python3 dd-rehydrate-past.py d1c7d0a8 7c5fa03b 20200714 10
	TOTAL OBJECTS : 0
	PROCESSED LINES COUNT : 0

###### Sample Processed Log Event

	{
	  "_id": "AXNMi2FNaGp-_-GTSYAA",
	  "date": "2020-01-15T08:57:48.000Z",
	  "service": "payment",
	  "host": "landfall-dehydrate",
	  "message": "Nectar Snare Psychic",
	  "status": "info",
	  "source": "redis",
	  "attributes": {
	    "env": "dev",
	    "duration": "2.13628",
	    "hostname": "landfall-dehydrate",
	    "provider": "ovh",
	    "service": "payment",
	    "id": "e2bb09df",
	    "region": "eu-west-1",
	    "operation": "write"
	  }
	}

#### Extra Tooling

##### Dummy Log Event Generator

Ship dummy log event using DD API

Depends on :

- `curl`
- `gdate` from `coreutils` brew package
- `diceware` `python3-pip` package

###### Usage

	# bash dd-ship-dummy-log.sh

###### Example Message

	{
	    "ddsource": "elasticsearch",
	    "ddtags": "env:int,service:checkout,provider:gcp,region:eu-west-1,operation:read,id:47640a2c",
	    "hostname": "emu-joylessly",
	    "duration": "4.4431",
	    "@timestamp": "2020-07-14T09:08:25.771Z",
	    "original_timestamp": "2020-01-15T09:08:25.000Z",
	    "message": "Lumpiness Freeness Result",
	    "env":"int",
	    "service":"checkout",
	    "provider":"gcp",
	    "region":"eu-west-1",
	    "operation":"read",
	    "id":"47640a2c"
	}

###### Credentials

	# cat .env
	DD_CLIENT_API_KEY=01****************************7e
	DD_CLIENT_APP_KEY=aa************************************cf
